import os
import tempfile
from contextlib import contextmanager
from typing import Generator

import nox

locations = "src", "tests", "noxfile.py"
nox.options.sessions = "lint", "safety", "tests"


@contextmanager
def temporary_file() -> Generator[str, None, None]:
    # On windows we cannot use tempfile.NamedTemporaryFile() directly because
    # the file cannot be written while still open
    tmpf = tempfile.NamedTemporaryFile(delete=False)
    tmpf.close()
    try:
        yield tmpf.name
    finally:
        os.unlink(tmpf.name)


@nox.session(python=["3.9", "3.8", "3.7"])
def tests(session):
    args = session.posargs or [
        "--cov",
        "--cov-report",
        "html:coverage_html",
        "-m",
        "not e2e",
    ]
    session.run("poetry", "install", "--no-dev", external=True)
    install_with_constraints(
        session, "coverage[toml]", "pytest", "pytest-cov", "pytest-mock"
    )
    session.run("pytest", *args)


@nox.session(python=["3.9", "3.8", "3.7"])
def lint(session):
    args = session.posargs or locations
    install_with_constraints(
        session,
        "flake8",
        "flake8-black",
        "flake8-bandit",
        "flake8-bugbear",
        "flake8-isort",
    )
    session.run("flake8", *args)


@nox.session(python=["3.9", "3.8"])
def black(session):
    args = session.posargs or locations
    session.install_with_constraints(session, "black")
    session.run("black", *args)


@nox.session(python=["3.9", "3.8"])
def safety(session):
    with temporary_file() as requirements:
        session.run(
            "poetry",
            "export",
            "--dev",
            "--format=requirements.txt",
            "--without-hashes",
            f"--output={requirements}",
            external=True,
        )
        install_with_constraints(session, "safety")
        session.run("safety", "check", f"--file={requirements}", "--full-report")


def install_with_constraints(session, *args, **kwargs):
    with temporary_file() as requirements:
        session.run(
            "poetry",
            "export",
            "--dev",
            "--format=requirements.txt",
            f"--output={requirements}",
            external=True,
        )
        session.install(f"--constraint={requirements}", *args, **kwargs)
